import requests
import time

print('Hi! Willkommen zur Donald Trump API!')

# time.sleep(1)

query = input('Was denkt Donald Trump über...')

response = requests.get('https://api.tronalddump.io/search/quote?query=' + query)

# converting to JSON Format
response = response.json()

# check if there is a result
if response['count'] > 0:
    # saving array of quotes
    quotes_list = response['_embedded']['quotes']
    # print(quotes_list)
    more = 'j'
    index = 0
    while more == 'j':
        text = quotes_list[index]['value']
        print(text)
        index += 1
        more = input('Möchtest du noch einen lesen? (j/n)')
    print('Bis dann!')
else:
    print('Sorry, aber dazu finden wir leider nichts :(')